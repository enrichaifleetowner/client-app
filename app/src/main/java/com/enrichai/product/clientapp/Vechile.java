package com.enrichai.product.clientapp;

/**
 * Created by WELCOME on 6/7/2018.
 */

public class Vechile {
    private String vechile_name;
    private String vechile_registration_code;

    public Vechile(String vechile_name, String vechile_registration_code){
        this.vechile_name = vechile_name;
        this.vechile_registration_code = vechile_registration_code;
    }

    public void setVechile_registration_code(String vechile_registration_code) {
        this.vechile_registration_code = vechile_registration_code;
    }

    public void setVechile_name(String vechile_name) {
        this.vechile_name = vechile_name;
    }

    public String getVechile_registration_code() {
        return vechile_registration_code;
    }

    public String getVechile_name() {
        return vechile_name;
    }
}
