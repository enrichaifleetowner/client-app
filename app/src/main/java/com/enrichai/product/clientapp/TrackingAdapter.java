package com.enrichai.product.clientapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by WELCOME on 6/7/2018.
 */

public class TrackingAdapter extends RecyclerView.Adapter<TrackingAdapter.VehicleViewHolder> {

    private ArrayList<Vechile> data;
    private static ArrayList<Boolean> isEnabled = new ArrayList();
    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String v_name;

    public static final String VEHICLE_ENABLED = "VEHICLE_ENABLED";

    public void enable(boolean status){
        isEnabled.add(status);
    }

    public TrackingAdapter(ArrayList<Vechile> data, Context context){
        this.data = data;
        this.context = context;
        sharedPreferences = this.context.getSharedPreferences("com.enrichai.product.clientapp", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        v_name = sharedPreferences.getString(VEHICLE_ENABLED, null);
        for(int k=0; k<this.data.size(); k++){
            if(this.data.get(k).getVechile_name().equals(v_name))
                isEnabled.add(true);
            else
                isEnabled.add(false);
        }
    }

    @Override
    public VehicleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_vehicle_item, parent, false);
        return new VehicleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final VehicleViewHolder holder, final int position) {
        final String vehicle_name = data.get(position).getVechile_name();
        String vehicle_registration_number = data.get(position).getVechile_registration_code();
        holder.bindValues(vehicle_name, vehicle_registration_number, position);
        holder.tb_toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    int index;
                    for(index=0; index<isEnabled.size(); index++)
                        if(isEnabled.get(index) == true && position != index) {
                            holder.tb_toggle.setChecked(false);
                            AlertDialog.Builder builder;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(context);
                            }
                            builder.setTitle("Warning")
                                    .setMessage("Please disable tracking service of "+data.get(index).getVechile_name())
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                            break;
                        }
                    if(index == isEnabled.size())
                        isEnabled.set(position, true);
                } else {
                    // The toggle is disabled
                    isEnabled.set(position, false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class VehicleViewHolder extends RecyclerView.ViewHolder {
        TextView tv_vehicle_name;
        TextView tv_vehicle_registration_number;
        ToggleButton tb_toggle;

        public VehicleViewHolder(View itemView) {
            super(itemView);
            tv_vehicle_name = itemView.findViewById(R.id.tv_vehicle_name);
            tv_vehicle_registration_number = itemView.findViewById(R.id.tv_vehicle_registration_number);
            tb_toggle = itemView.findViewById(R.id.toggle_button);
        }

        private void bindValues(String vehicle_name, String vehicle_registration_number, int index){
            tv_vehicle_name.setText(vehicle_name);
            tv_vehicle_registration_number.setText(vehicle_registration_number);
            tb_toggle.setChecked(isEnabled.get(index));
        }
    }
}
