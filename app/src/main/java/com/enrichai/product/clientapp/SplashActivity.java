package com.enrichai.product.clientapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by WELCOME on 6/7/2018.
 */

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_TIME_OUT = 3000;
    public static final String KEY_LOGIN_STATUS = "login_status";
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        sharedPreferences = getSharedPreferences("com.enrichai.product.clientapp", Context.MODE_PRIVATE);
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                // Check under utils file about Preferences..!!
                //Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                /*SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(KEY_LOGIN_STATUS, true);
                editor.commit();*/
                Intent intent;
                Log.d(KEY_LOGIN_STATUS, sharedPreferences.getBoolean(KEY_LOGIN_STATUS, false)+"");
                if(sharedPreferences.getBoolean(KEY_LOGIN_STATUS, false))
                    intent = new Intent(SplashActivity.this, TrackingDashboard.class);
                else
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
