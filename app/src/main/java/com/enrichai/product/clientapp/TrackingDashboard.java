package com.enrichai.product.clientapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by WELCOME on 6/7/2018.
 */

public class TrackingDashboard extends AppCompatActivity {

    final private String TAG = TrackingDashboard.class.getSimpleName();

    private ArrayList<Vechile> list_of_vechile;
    private FloatingActionButton floatingActionButton;
    private EditText ev_vechile_name;
    private EditText ev_vechile_registration_number;
    private GridLayoutManager mLayoutManager;
    private RecyclerView gvContentView;
    private TrackingAdapter trackingAdapter;
    private Button bt_create;
    private Button bt_close;
    private TextView tv_empty_list;
    public static final String KEY_LOGIN_STATUS = "login_status";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tracking_dashboard);

        initialize();

        if(list_of_vechile.size() == 0)
            showEmptyList();
        else
            hideEmptyList();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addNewVechile();
            }
        });
        mLayoutManager = new GridLayoutManager(this, 1);
        gvContentView.setLayoutManager(mLayoutManager);
        trackingAdapter = new TrackingAdapter(list_of_vechile, this);
        gvContentView.setAdapter(trackingAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemSelected = item.getItemId();
        if(menuItemSelected == R.id.action_logout) {
            Intent intent = new Intent(TrackingDashboard.this, LoginActivity.class);
            SharedPreferences sharedPreferences = getSharedPreferences("com.enrichai.product.clientapp", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(KEY_LOGIN_STATUS, false);
            editor.apply();
            Log.d(KEY_LOGIN_STATUS, sharedPreferences.getBoolean(KEY_LOGIN_STATUS, false)+"");
            startActivity(intent);
            overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addNewVechile(){
        final Dialog dialog = new Dialog(TrackingDashboard.this);
        dialog.setContentView(R.layout.add_vechile);

        ev_vechile_name = dialog.findViewById(R.id.ev_vechile_name);
        ev_vechile_registration_number = dialog.findViewById(R.id.ev_vechile_number);
        bt_create = dialog.findViewById(R.id.add_new_vechile);
        bt_close = dialog.findViewById(R.id.close);

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        bt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        bt_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String vechile_name = ev_vechile_name.getText().toString().trim();
                String vechile_registration_number = ev_vechile_registration_number.getText().toString().trim();
                if(vechile_name.isEmpty())
                    ev_vechile_name.setError("Field can not be empty");
                if(vechile_registration_number.isEmpty() && !vechile_name.isEmpty()) {
                    ev_vechile_registration_number.requestFocus();
                    ev_vechile_registration_number.setError("Field can not be empty");
                }
                if(!vechile_name.isEmpty() && !vechile_registration_number.isEmpty()){
                    Vechile new_vechile = new Vechile(vechile_name, vechile_registration_number);
                    list_of_vechile.add(new_vechile);
                    if(list_of_vechile.size() == 1)
                        hideEmptyList();
                    trackingAdapter.enable(false);
                    trackingAdapter.notifyDataSetChanged();
                    Log.d(TAG, "#"+list_of_vechile.size()+" - New Vechile Added");
                    Toast.makeText(view.getContext(), "New Vechile Added", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }
        });
    }

    public void initialize(){
        list_of_vechile = new ArrayList();
        floatingActionButton = findViewById(R.id.fab);
        gvContentView = findViewById(R.id.r_view);
        tv_empty_list = findViewById(R.id.tv_empty_list);
    }

    private void hideEmptyList(){
        tv_empty_list.setVisibility(View.INVISIBLE);
        gvContentView.setVisibility(View.VISIBLE);
    }

    private void showEmptyList(){
        tv_empty_list.setVisibility(View.VISIBLE);
        gvContentView.setVisibility(View.INVISIBLE);
    }
}
