package com.enrichai.product.clientapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by WELCOME on 6/7/2018.
 */

public class LoginActivity extends AppCompatActivity{

    private EditText et_username;
    private EditText et_password;
    private String password;
    private String username;
    private String USERNAME = "admin";
    private String PASSWORD = USERNAME;
    private TextView tv_login;
    public static final String KEY_LOGIN_STATUS = "login_status";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        tv_login = findViewById(R.id.tv_login);
        et_username = findViewById(R.id.etUserName);
        et_password = findViewById(R.id.etPassword);

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if(USERNAME.equals(username) && PASSWORD.equals(password)){
                    SharedPreferences sharedPreferences = getSharedPreferences("com.enrichai.product.clientapp", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean(KEY_LOGIN_STATUS, true);
                    editor.apply();
                    Log.d(KEY_LOGIN_STATUS, sharedPreferences.getBoolean(KEY_LOGIN_STATUS, false)+"");
                }
                Intent intent = new Intent(LoginActivity.this, TrackingDashboard.class);
                startActivity(intent);
                finish();*/
                username = et_username.getText().toString().trim();
                password = et_password.getText().toString().trim();
                if(USERNAME.equals(username) && PASSWORD.equals(password)){
                    SharedPreferences sharedPreferences = getSharedPreferences("com.enrichai.product.clientapp", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean(KEY_LOGIN_STATUS, true);
                    editor.apply();
                    Log.d(KEY_LOGIN_STATUS, sharedPreferences.getBoolean(KEY_LOGIN_STATUS, false)+"");
                    Intent intent = new Intent(LoginActivity.this, TrackingDashboard.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    Toast.makeText(view.getContext(),"Invalid Email/Password",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
